import numpy as np
from random import randint

def generateESTParams(topicsCount, dictionarySize):
	topicsDistribution = np.ones(topicsCount) / topicsCount
	topicWordsDistribution  = []
	
	for topicID in range(0, topicsCount):
		wordsDist = np.random.random(dictionarySize)
		wordsDist /= np.sum(wordsDist)
		topicWordsDistribution.append(wordsDist)
	return (topicsDistribution, np.array(topicWordsDistribution))

def generateESTWords(topicsDistribution, topicWordsDistribution, topicsCount, minDocSize, maxDocSize):
	documentSize   = randint(minDocSize, maxDocSize)
	dictionarySize = np.size(topicWordsDistribution[0])
	
	x_vecs = []
	selectedTopic = np.random.choice(np.arange(0, topicsCount), p=topicsDistribution)
	wordsDist    = topicWordsDistribution[selectedTopic]
	for wordPos in range(0, documentSize):
		selectedWord = np.random.choice(np.arange(0, dictionarySize), p=wordsDist)
		y = np.zeros(dictionarySize)
		y[selectedWord] = 1
		x_vecs.append(y)
	
	return  np.array(x_vecs)

if __name__ == '__main__':
	topicsCount = 5
	dictionarySize = 10
	maxDocSize = 10
	
	(topics, topicWords) = generateESTParams(topicsCount, dictionarySize)
	x_vecs = generateESTWords(topics, topicWords, topicsCount, maxDocSize, maxDocSize)
	print('Params: No of topics: ', topicsCount, ' No of words: ', dictionarySize, 'Document size: ', maxDocSize)
	print('Topics distribution (W):', topics)
	print('Topic words distributions (Mu_1,...,Mu_k): ', topicWords)
	
	print('Words: ')
	print(x_vecs)
