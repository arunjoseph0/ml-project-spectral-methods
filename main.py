from generateGMM import *
from generateLDA import *
from ecaGMM_main import *
from ecaLDA_main import *
def main():
    topicCount = 5;
    documentCount = 1;
    dictionarySize = 10;
    documentList = [];
    alpha, betaParams = generateLDAParams(topicCount, dictionarySize);
    for docID in range(0, documentCount):
        documentList.append(generateLDAWords(alpha, betaParams, topicCount, 10));
    for docID in range(0, 1):
        wordVectors = documentList[docID];
        alpha0 = 0.3#sum(alpha);
        O = ecaLDA(np.array(wordVectors), topicCount, alpha0);
        print(betaParams)
        print(O);
        print(alpha)
        alphaEst = estimateAlpha(np.array(wordVectors), O, alpha0,topicCount );
    '''
    meanParams, covParams = generateGaussianParams(topicCount, dictionarySize);
    for docID in range(0, documentCount):
        documentList.append(generateWords(meanParams, covParams, topicCount,5));

    text_file = open("dictionary", "r")
    wordList = text_file.read().split('\n')

    for docID in range(0, documentCount):
        wordVectors = documentList[docID];
        O = ecaGMM(np.array(wordVectors), topicCount);
        #print(O);
    '''



if __name__ == "__main__":
    main()
