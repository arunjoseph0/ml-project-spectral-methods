import time
from tensor_main_est import *
import numpy as np


dictionarySize = 50
topicsCount = 3
minDocSize = 400
maxDocSize = 700
'''
topics, topicWords = generateESTParams(topicsCount, dictionarySize)



startRange = 10
documentList = []
for docID in range(0, startRange):
    documentList.append(generateESTWords(topics, topicWords, topicsCount, minDocSize, maxDocSize))
print('Documents Generated')
#f = open('EST_out_v2.txt', 'a')
f = open('out.txt', 'a')
f.write('Exchangable Single Topic Model \n')
f.write('-------------------------------\n\n')
f.write('Documents #        Time Taken     Topic Prob Error    Word Prob Error\n')
print('EST Begins')
docCountList = [100]
prevDocCount = docCountList[0]
for documentCount in docCountList:
    start = time.clock()
    [topic_err, dist_err] = tensor_main_est(topicsCount, topics, topicWords, documentList)
    time_taken = time.clock() - start
    print ('Documents # : ', documentCount, ' Time : ', time_taken, ' Topic Err :', topic_err, ' Distr Err : ', dist_err)
    f.write(''.join([str(documentCount), '       ', str(time_taken),'      ', str(topic_err),'     ', str(dist_err), '\n']))
    for docID in range(prevDocCount, documentCount):
        documentList.append(generateESTWords(topics, topicWords, topicsCount, minDocSize, maxDocSize))
    print(np.size(documentList, 0))
    prevDocCount = documentCount
f.close()
'''

documentCount = 1000
print('Documents Generated')
#f = open('EST_out_v2.txt', 'a')
f = open('EST_out_v2.txt', 'a')
f.write('Exchangable Single Topic Model - Vary Dictionary Size\n')
f.write('-------------------------------\n\n')
f.write('DictionarySize #        Time Taken     Topic Prob Error    Word Prob Error\n')
print('EST Begins')

dicSizeList = [50, 100, 200, 400, 800, 1000]
for dictionarySize in dicSizeList:
    documentList = []
    topics, topicWords = generateESTParams(topicsCount, dictionarySize)
    for docID in range(0, documentCount):
        documentList.append(generateESTWords(topics, topicWords, topicsCount, minDocSize, maxDocSize))
    
    start = time.clock()
    [topic_err, dist_err] = tensor_main_est(topicsCount, topics, topicWords, documentList)
    time_taken = time.clock() - start
    print ('DictionarySize # : ', dictionarySize, ' Time : ', time_taken, ' Topic Err :', topic_err, ' Distr Err : ', dist_err)
    f.write(''.join([str(dictionarySize), '       ', str(time_taken),'      ', str(topic_err),'     ', str(dist_err), '\n']))
f.close()