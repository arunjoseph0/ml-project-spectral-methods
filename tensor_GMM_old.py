import numpy as np

#generates the matrix E[x * x] for spherical GMM
#Refer Theorem 3.2
def generate_tensor_pairs(X):
    d = len(X[0]); # dictioanry size
    n = len(X); # no of words in the document
    pairs = np.zeros((d,d));
    for i in range(0, n):
         for j in range(0, n):
             if i != j:
                 pairs = pairs + np.add(pairs, np.outer(X[i], X[j]));
    pairs = pairs / (n*n);
    return pairs;

#generates the matrix M2 for spherical GMM
#Refer Theorem 3.2
def generate_M2(X, sigma):
    d = len(X[0]);
    pairs = generate_tensor_pairs_GMM(X);
    M2 = pairs - sigma**2 * np.identity(d);
    return M2;

#generates the matrix M3 for spherical GMMs
#Refer Theorem 3.2
def generate_M3(X, M1, sigma):
    d = len(X[0]) # dictioanry size
    n = len(X) # no of words in the document
    M3 = np.zeros((d, d, d))
    for i in range(0,n):
        for j in range(0,n):
            for k in range(0,n):
                M3 = M3 + np.tensordot(np.tensordot(X[i], X[j], axes = 0), X[k], axes = 0);
    M3 = M3 / (n*n*n);
    term = np.zeros((d, d, d));
    for i in range(0, d):
        ei = np.zeros(d);
        ei[i] = 1;
        term = term + np.tensordot(np.tensordot(M1, ei, axes = 0), ei, axes = 0);
        term = term + np.tensordot(np.tensordot(ei, M1, axes = 0), ei, axes = 0);
        term = term + np.tensordot(np.tensordot(ei, ei, axes = 0), M1, axes = 0);
    M3 = M3 - sigma**2 * term;
    return M3;

#Computes the sigma parameter which is used to calculate the M3 matrix for spherical gaussians
#Refer Theorem 3.2
def estimate_variance(mean, pairs):
    sigma_mat = pairs - np.tensordot(mean, mean, axes =0);
    eig_val, eig_vect = np.linalg.eig(sigma_mat);
    smallest_eig_val = np.amin(eig_val);
    return smallest_eig_val;

def generate_input_matrices(X):
    d = len(X[0]);
    n = len(X);
    M1 = X.sum(axis = 0)/ n;
    tensor_pairs = generate_tensor_pairs_GMM(X);
    sigma = find_sigma(M1, tensor_pairs);
    M2 = generate_M2_GMM(X, sigma);
    M3 = generate_M3_GMM(X, M1, sigma);
    return M3, M2, M1
