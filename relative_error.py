import itertools
import numpy as np



def find_relative_error(X, Y):
    no_cols = len(X);
    dim = len(X[0]);
    index_perm = list(itertools.permutations(list(range(no_cols))))
    total_perm = len(index_perm)
    X = np.matrix(X)
    error_list = []
    for ind in range(0, total_perm):
        Z = X[index_perm[ind], :]
        error_list.append(np.linalg.norm(Z - Y, ord = 'fro'))
    error_list = np.array(error_list)
    min_error = error_list.min()
    min_err_ind = np.where(error_list == min_error)[0][0]

    #print(error_list, min_error, min_err_ind)
    #print(index_perm[min_err_ind])
    return min_error, X[index_perm[min_err_ind], :]


#find_relative_error([[1, 1, 1], [2, 2, 2], [3, 3, 3], [4,4,4]], [[2, 2, 2], [1, 1, 1], [4, 4, 4], [3,3,3]])