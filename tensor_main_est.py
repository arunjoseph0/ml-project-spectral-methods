import sys
from relative_error import *

from tensor_decomposition import *
from tensor_reduction import *

from tensor_EST import *
from generateEST import *

def tensor_main_est(topicsCount , topics, topicWords, documentList):
	#documentList = []
    #
	# print('TENSOR DECOMPOSITION: EST MODEL')
	# print('---------------------------------')
    #
	# print('No of topics: ', topicsCount)
	# print('No of words in vocab: ', dictionarySize)
	# print('No of documents: ', documentCount)
	# print('No of words/doc: ', minDocSize, '...', maxDocSize)
	# print('')
    #
	# print('ESTIMATING MOMENTS FROM DATA')
	# print('-------------------------------')
	est = TensorEST()
	# topics, topicWords = generateESTParams(topicsCount, dictionarySize)
    #
	# for docID in range(0, documentCount):
	# 	documentList.append(generateESTWords(topics, topicWords, topicsCount, minDocSize, maxDocSize))

	# Computing the params for tensor decomposition
	# print('Calculating moments: M1, M2, M3')

	M3, M2, M1 = est.generate_moment_estimates(documentList)

	M2_ideal = est.calculate_M2_ideal(topicsCount, topics, topicWords)
	M3_ideal = est.calculate_M3_ideal(topicsCount, topics, topicWords)

	# print('M2 is symmetric : ', np.array_equal(M2, np.transpose(M2)))
	lmbda = np.linalg.eigvalsh(M2)
	# print('Eigenvalues of M2:', lmbda)

	# print ('M2_ideal is symmetric : ', np.array_equal(M2_ideal, np.transpose(M2_ideal)))
	lmbda2 = np.linalg.eigvalsh(M2_ideal)
	# print('Eigenvalues of M2_ideal:', lmbda2)

	# print('F-norm of residual (M2 - M2_ideal):', np.linalg.norm(M2 - M2_ideal, ord='fro'))
	# print('Rank of M2:' , np.linalg.matrix_rank(M2), ' Rank of M2_ideal:' , np.linalg.matrix_rank(M2_ideal))
	#
	# print('M3 is symmetric : ', check_symmetry(M3, (dictionarySize, dictionarySize, dictionarySize)))
	#
	# print('M3_ideal is symmetric : ', check_symmetry(M3_ideal, (dictionarySize, dictionarySize, dictionarySize)))
	#
	# print('F-norm of residual (M3 - M3_ideal):', tensor3_frobenius(M3 - M3_ideal))
	#
	# print('Finding M3_tilde...')
	M3_tilde, B = tensor_reduction(M2, M3, topicsCount)

	#
	# print('\nROBUST TENSOR DECOMPOSITION')
	# print('-------------------------------')
	#
	# print('Performing robust tensor decomposition on M3_tilde...')
	T_cap = np.copy(M3_tilde)
	L = topicsCount * topicsCount
	N = 100
	lambdas = []
	thetas = []
	T_approx = np.zeros((topicsCount, topicsCount, topicsCount))
	for iteration in range(1, topicsCount + 1):
		(lambda_cap, theta_cap) = robust_tensor_decomposition(T_cap, topicsCount, L, N)
		r = lambda_cap * tensor3_product(theta_cap)
		T_approx = T_approx + r
		#print('Appromixation error : ', tensor3_frobenius(r))
		lambdas.append(lambda_cap)
		thetas.append(theta_cap)
		T_cap -= r
		#print('robust_tensor_decomposition iteration complete: ', iteration)

	# print('\nRECONSTRUCTION')
	# print('----------------')

	# Theorem 4.3 in [Anandkumar14b] => lambda_i = 1 / sqrt(w_i)
	W_est = 1 / (np.array(lambdas))**2

	# print('Estimated W: ', W_est)
	# print('Original W: ', topics)
	W_err = np.linalg.norm(W_est - topics)

	Mu_est = []
	# Theorem 4.3 in [Anandkumar14b] => Mu_i = lambda_i * B * theta_i
	for i in range(0, topicsCount):
		Mu_est.append(lambdas[i] * np.matmul(B, thetas[i]))

	# print('Relative error (Frobenius-norm) of estimated and actual Mu:')
	rel_error, Mu_reordered = find_relative_error(Mu_est, topicWords)
	# print(rel_error)
	return W_err, rel_error
	#print('Relative error (element-wise) of estimated and actual Mu:')
	#print(np.multiply(Mu_reordered - topicWords, 1 / topicWords))
if __name__ == "__main__":
	tensor_main_est()
