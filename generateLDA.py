import numpy as np
from random import randint

def generateLDAParams(topicsCount, dictionarySize):
	alpha = np.array([0.7, 0.2, 0.1])
	#alpha = np.ones(topicsCount) / topicsCount # Ensure alpha vector's sum = our selected conc. alpha = 1
	beta  = []
	
	for topicID in range(0, topicsCount):
		betaDist = np.random.random(dictionarySize)
		betaDist /= np.sum(betaDist)
		beta.append(betaDist)
	return (alpha, np.array(beta))

def generateLDAWords(alpha, beta, topicsCount, minDocSize, maxDocSize):
	documentSize   = randint(minDocSize, maxDocSize)
	dictionarySize = np.size(beta[0])
	
	x_vecs = []
	topicsDistribution = np.random.dirichlet(alpha)
	for wordPos in range(0, documentSize):
		selectedTopic = np.random.choice(np.arange(0, topicsCount), p=topicsDistribution)
		betaDist    = beta[selectedTopic]
		selectedWord = np.random.choice(np.arange(0, dictionarySize), p=betaDist)
		y = np.zeros(dictionarySize)
		y[selectedWord] = 1
		x_vecs.append(y)
	
	return  np.array(x_vecs)

if __name__ == '__main__':
	topicsCount = 5
	dictionarySize = 10
	maxDocSize = 10
	
	(alpha, beta) = generateLDAParams(topicsCount, dictionarySize)
	x_vecs = generateLDAWords(alpha, beta, topicsCount, maxDocSize, maxDocSize)
	print('Params: No of topics: ', topicsCount, ' No of words: ', dictionarySize, 'Document size: ', maxDocSize)
	print('Dirichlet prior (alpha):', alpha)
	print('Topic words distributions (beta): ', beta)
	
	print('Words: ')
	print(x_vecs)
