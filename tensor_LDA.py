import numpy as np

class TensorLDA:

    #generates the matrix E[x * x] for LDA
    #Refer Theorem 3.5
    def generate_M2(self, X):
        d = np.size(X, 1)  # dictionary size
        n = np.size(X, 0)  # no of words in the document
        M2 = np.zeros((d, d))
        C = np.sum(X, axis=0)

        for i in range(0, d):
            for j in range(0, d):
                M2[i][j] = C[i] * C[j]
        M2 = M2 / (1.0 * n * n)
        return M2

    # calculates M2 matrix from the word distributions mu obtained from LDA
    def calculate_M2_ideal(self, Mu, k, alpha):
        d = np.size(Mu, 1)  # Dictionary size
        alpha0 = sum(alpha) # not sure if alpha0 has to be computed this way, or to use the alpha0 used for estimation of M2
        M2 = np.zeros((d, d))

        for i in range(0, k):
            mu = np.array(Mu[i])
            w = (alpha[i] / ((alpha0 + 1) * alpha0))
            M2 = np.add(M2, w * np.outer(mu, mu))
        return M2

    # generates the matrix M3 for LDA
    # Refer Theorem 3.5
    def generate_M3(self, X):
        d = np.size(X, 1)  # dictionary size
        n = np.size(X, 0)  # no of words in the document
        M3 = np.zeros((d, d, d))
        C = np.sum(X, axis=0)

        for i in range(0, d):
            for j in range(0, d):
                for k in range(0, d):
                    M3[i][j][k] = C[i] * C[j] * C[k]
        M3 = M3 / (1.0 * n * n * n)
        return M3

    # calculates M3 matrix from the word distributions mu obtained from LDA and the LDA parameters
    def calculate_M3_ideal(self, Mu, k, alpha):
        d = np.size(Mu, 1)  # Dictionary size
        alpha0 = sum(alpha)
        print('Alpha0 : ', alpha0)
        M3 = np.zeros((d, d, d))

        for i in range(0, k):
            W = (2 * alpha[i] / ((alpha0 + 2) * (alpha0 + 1) * alpha0))
            M3 = np.add(M3, W * np.tensordot(np.tensordot(Mu[i], Mu[i], axes=0), Mu[i], axes=0))
        return M3

    def estimate_alpha(self, W):
        C = sum(W)
        coeff = [1, 3, 2-2/C]
        roots = np.roots(coeff)
        print('Roots : ', roots)
        alpha0 = max(roots)
        denomConst = ((alpha0 + 2)* (alpha0 + 1)*alpha0)/2
        alphas = denomConst * W
        return alphas


    # Computes and returns M3, M2, M1 (in that order) from the data for a set of documents
    def generate_moment_estimates(self, X_list, alpha0):
        documentCount = len(X_list)  # No of documents
        d = np.size(X_list[0][0])  # Dictionary size
        M1 = np.zeros(d)
        M2 = np.zeros((d, d))
        M3 = np.zeros((d, d, d))

        for i in range(0, documentCount):
            X = X_list[i]
            n = np.size(X, 0)  # No of words in document i
            M1 = np.add(M1, np.sum(X, axis=0) / n)
            M2 = np.add(M2, self.generate_M2(X))
            M3 = np.add(M3, self.generate_M3(X))

        M1 = M1 / documentCount
        M2 = M2 / documentCount
        M3 = M3 / documentCount

        alpha_const = (alpha0 / (alpha0 + 1))
        M2 = M2 - alpha_const * np.outer(M1, M1)

        M3_b = np.zeros((d, d, d))
        C = np.sum(X, axis=0)
        CM = np.add(C, M1)
        #EM_1 = np.zeros((d, d))
        #EM_2 = np.zeros((d, d))
        #EM_3 = np.zeros((d, d))
        for i in range(0, d):
            for j in range(0, d):
                for k in range(0, d):
                    M3_b[i][j][k] = CM[i] * CM[j] * CM[k]

        M3_b = M3_b / (1.0 * n * n * n)

        M3_b = (alpha0/ (alpha0 + 2)) * M3_b
        alpha_coeff = (2 * alpha0 * alpha0/((alpha0 + 2) * (alpha0 + 1)))
        M3_b = M3_b - alpha_coeff * np.tensordot(np.tensordot(M1, M1, axes=0), M1, axes=0)
        M3 = M3 - M3_b

        return M3, M2, M1