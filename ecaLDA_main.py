import numpy as np
from generateLDA import *

# Generates the matrix pairs used in ECA algorithm for LDA from the data X (for a document)
# Refer Anandkumar et al. 2013(A spectral algorithm for LDA), Section 4.3
def findPairsLDA(X, mean, alpha0):
    d = np.size(X, 1) # dictionary size
    n = np.size(X, 0) # no of words in the document
    pairs = np.zeros((d,d))
    for i in range(0, n):
        for j in range(0, n):
                pairs += np.outer(X[i], X[j])
    pairs= pairs / n*n
    pairs = pairs - (alpha0/ (alpha0 + 1)) * np.outer(mean, mean)
    return pairs

# Generates the matrix triples used in ECA algorithm for LDA from the data X (for a document)
# Refer Anandkumar et al. 2013(A spectral algorithm for LDA), Section 4.3
def findTriplesLDA(X, mean, alpha0, eta):
    d = np.size(X, 1) # dictionary size
    n = np.size(X, 0) # no of words in the document
    expectX1X2 = np.zeros((d, d))
    for i in range(0, n):
        for j in range(0, n):
            expectX1X2 += np.outer(X[i], X[j])
    expectX1X2 = expectX1X2 / (n*n)

    term1 = np.zeros((d,d))
    for i in range(0,n):
        for j in range(0,n):
            for k in range(0,n):
                term1 += np.outer(X[i], X[j]) * np.inner(X[k], eta)

    term1 = term1 / (n * n * n)
    term2 = np.matmul(np.matmul(expectX1X2, eta), np.transpose(mean))
    term3 = np.matmul(np.outer(mean,  eta), expectX1X2)
    term4 = np.inner(eta, mean) * expectX1X2
    term5 = np.inner(eta, mean)* np.outer(mean, mean)
    triples = term1 - (alpha0/(alpha0+2))*(term2 + term3 + term4) +  ((2*alpha0*alpha0)/((alpha0+1)*(alpha0+2))) * term5
    return triples

# Estimate the topic distribution alpha from alpha0 and the estimated topic-word distribution O
def estimateAlpha(X, O, alpha0, k):
    n = np.size(X, 0)  # no of words in the document
    mean = X.sum(axis = 0)/n
    pairs = findPairsLDA(X, mean, alpha0)
    Oinv = np.linalg.pinv(O)
    #t1 = np.matmul(Oinv, pairs)
    #t2 = np.matmul(t1, np.transpose(Oinv))
    #t3 = np.matmul(t2, np.transpose(np.ones(n)));
    alpha = alpha0*(alpha0 + 1) * np.matmul(np.matmul(np.matmul(Oinv, pairs) , np.transpose(Oinv)) , np.ones(k))
    return alpha


# ECA algorithm(for LDA) to estimate topic word distribution O
# Refer Anandkumar et al. 2013(A spectral algorithm for LDA), Algorithm 3
def ecaLDA(X, k, alpha0):
    d = np.size(X, 1) # dictionary size
    n = np.size(X, 0) # no of words in the document
    mean = X.sum(axis = 0)/n
    theta = np.random.rand(k)  #kx1
    pairs = findPairsLDA(X, mean, alpha0)

    #Step 1
    randTheta = np.random.randn(d, k) #dxk
    U = np.matmul(pairs, randTheta) #dxk

    #Step 2
    Ut = U.transpose() #kxd
    Y = np.matmul(np.matmul(Ut,pairs),U) #kxk
    sigma,V1 = np.linalg.eig(Y)
    Vmul = np.matmul(V1,np.diag(np.sqrt(sigma)))
    V = np.transpose(np.linalg.pinv(Vmul))
    W = np.matmul(U,V) #dxk

    #step 3
    eta = np.matmul(W,theta) #dx1
    triples = findTriplesLDA (X, mean, alpha0, eta)
    Wt = W.transpose() #kxd
    svMat = np.matmul(np.matmul(Wt,triples), W) #kxk
    leftSV, SV, rightSV = np.linalg.svd(svMat)

    #step 4
    pseudoWt = np.linalg.pinv(W).transpose()
    O=[]
    for E in leftSV.transpose():
        O_col = np.matmul(pseudoWt,E)
        O.append(O_col/np.sum(O_col))

    return np.array(O).transpose()

# The main program
if __name__ == "__main__":

    topicCount = 5
    documentCount = 1
    dictionarySize = 10
    maxDocSize = 100
    documentList = []
    alpha, beta = generateLDAParams(topicCount, dictionarySize)
    for docID in range(0, documentCount):
        documentList.append(generateLDAWords(alpha, beta, topicCount, maxDocSize, maxDocSize));
    for docID in range(0, 1):
        wordVectors = documentList[docID]
        alpha0 = 0.3#sum(alpha);
        O = ecaLDA(np.array(wordVectors), topicCount, alpha0)
        alphaEst = estimateAlpha(np.array(wordVectors), O, alpha0,topicCount )
