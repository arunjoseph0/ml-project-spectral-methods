import numpy as np

class TensorEST:
	# Generates the matrix M2 for EST model from the data X (for a document)
	# Refer [Anandkumar14b] Theorem 3.1 (first part)
	def generate_M2(self, X):
		d = np.size(X, 1) # dictionary size
		n = np.size(X, 0) # no of words in the document
		M2 = np.zeros((d,d))
		C = np.sum(X, axis = 0)
		
		for i in range(0, d):
			for j in range(0, d):
				M2[i][j] = C[i] * C[j]
		M2 = M2 / (1.0*n*n)
		return M2
	
	# Calculates the M2 matrix for EST model from the "known" model params (as we are using synthetic data)
	# This is using the second part of Theorem 3.1 from [Anandkumar14b]
	def calculate_M2_ideal(self, k, W, Mu):
		d = np.size(Mu, 1) # Dictionary size
		M2 = np.zeros((d, d))
		
		for i in range(0, k):
			mu = np.array(Mu[i])
			M2 = np.add(M2, W[i] * np.outer(mu, mu))
		return M2
	
	# Generates the tensor M3 for EST model from the data X (for a document)
	# Refer [Anandkumar14b] Theorem 3.1 (first part)
	def generate_M3(self, X):
		d = np.size(X, 1) # dictionary size
		n = np.size(X, 0) # no of words in the document
		M3 = np.zeros((d, d, d))
		C = np.sum(X, axis = 0)
		
		for i in range(0, d):
			for j in range(0, d):
				for k in range(0, d):
					M3[i][j][k] = C[i] * C[j] * C[k]
		M3 = M3 / (1.0*n*n*n)
		return M3
	
	# Calculates the M3 tensor for EST model from the "known" model params (as we are using synthetic data)
	# This is using the second part of Theorem 3.1 from [Anandkumar14b]
	def calculate_M3_ideal(self, k, W, Mu):
		d = np.size(Mu, 1) # Dictionary size
		M3 = np.zeros((d, d, d))
		
		for i in range(0, k):
			M3 = np.add(M3, W[i] *  np.tensordot(np.tensordot(Mu[i], Mu[i], axes = 0), Mu[i], axes = 0))
		return M3
	
	# Computes and returns M3, M2, M1 (in that order) from the data for a set of documents
	def generate_moment_estimates(self, X_list):
		documentCount = len(X_list) # No of documents
		d = np.size(X_list[0][0]) # Dictionary size
		M1 = np.zeros(d)
		M2 = np.zeros((d, d))
		M3 = np.zeros((d, d, d))
		
		for i in range(0, documentCount):
			X = X_list[i]
			n = np.size(X, 0) # No of words in document i
			M1 = np.add(M1, np.sum(X, axis = 0) / n)
			M2 = np.add(M2, self.generate_M2(X))
			M3 = np.add(M3, self.generate_M3(X))
		
		M1 = M1 / documentCount
		M2 = M2 / documentCount
		M3 = M3 / documentCount
		
		return M3, M2, M1

if __name__ == '__main__':
	print('This file contains code that calculates the moments for EST model.')
