import numpy as np
import itertools as itertools
import random as random

# Generate a random symmetric tensor T (with order tuple 'order')
def gen_symmetric_tensor(order):
    k = order[0]
    p = np.size(order)
    T = np.zeros(order)

    for b in range(0, k**p):
        ind = np.zeros(p)
        b_n = b
        for j in range(p):
            ind[j] = b_n % k
            b_n = b_n / k
        if all(f <= l for f, l in zip(ind, ind[1:])):
            v = random.uniform(0, 100)
            for perm in itertools.permutations(ind):
                perm = np.asarray(perm).astype(int)
                T[tuple(perm)] = v
    return T

# Check if the given tensor T (with order tuple 'order') is
# a symmetric tensor
def check_symmetry(T, order):
    k = order[0]
    p = np.size(order)
    T = np.zeros(order)

    for b in range(0, k**p):
        ind = np.zeros(p)
        b_n = b
        for j in range(p):
            ind[j] = b_n % k
            b_n = b_n / k
        if all(f <= l for f, l in zip(ind, ind[1:])):
            ind = ind.astype(int)
            v = T[tuple(ind)]
            for perm in itertools.permutations(ind):
                perm = np.asarray(perm).astype(int)
                if T[tuple(perm)] != v:
                    return False
    return True

# Randomly sample a vector v in R^d on the unit sphere S^d
# This method works due to the spherical symmetry of the multivariate
# normal distribution (in d dimensions)
def sample_unit_sphere(d):
    vec = np.random.randn(d)
    vec /= np.linalg.norm(vec, axis=0)

    return np.transpose(vec)

# Compute and return the (normalized) vector T(I, theta, theta) in R^d, where T in R^{d*d*d}
def tp_iteration_update(T, theta):
    d = np.size(theta, 0)
    # Check if the tensor T has symmetric dim and dimensions of T and theta matches
    if d != np.size(T, 0) or np.size(T, 0) != np.size(T, 1) or np.size(T, 1) != np.size(T, 2):
        raise TypeError('tp_iteration_update: Dimension mismatch in tensor.')

    theta_new = np.zeros(d)
    for i in range(0, d):
        for j in range(0, d):
            for l in range(0, d):
                alpha  = T[i][j][l]
                alpha *= theta[j]
                alpha *= theta[l]
                theta_new = theta_new + alpha * (np.eye(1, d, i).flatten())

    theta_new /= np.linalg.norm(theta_new, axis=0)
    #print('theta = ', theta, 'theta_new=', theta_new)
    return theta_new

# Compute and return (scalar) T(u, u, u) where V = R^d, T: V^3 -> F
def tensor3_evaluate(T, u):
    d = np.size(u)

    # Check if the tensor T has symmetric dim and dimensions of T and theta matches
    if d != np.size(T, 0) or np.size(T, 0) != np.size(T, 1) or np.size(T, 1) != np.size(T, 2):
        raise TypeError('tensor_evaluate: Dimension mismatch in tensor.')

    r = 0.0
    for i in range(0, d):
        for j in range(0, d):
            for k in range(0, d):
                r += T[i][j][k] * u[i] * u[j] * u[k]
    return r

# Compute and return the symmetric order-3 tensor u x u x u in R^{d*d*d}, where u in R^d
def tensor3_product(u):
    d = np.size(u)

    T = np.zeros((d, d, d))
    for i in range(0, d):
        for j in range(0, d):
            for k in range(0, d):
                T[i][j][k] = u[i] * u[j] * u[k]
    return T

# Compute and return the Frobenius norm of the symmetric order-3 tensor T in R^{d*d*d}
def tensor3_frobenius(T):
    d = np.size(T, 0)

    F = 0.0
    for i in range(0, d):
        for j in range(0, d):
            for k in range(0, d):
                F += T[i][j][k]**2
    return np.sqrt(F)

# The Robust Tensor Decomposition algorithm (Algorithm 1 in Anandkumar et al. 2014)
def robust_tensor_decomposition(T, k, L, N):
    thetas = {}
    tm_vals = np.zeros(L)
    for tau in range(0, L):
        theta = sample_unit_sphere(k);
        for t in range(0, N):
            theta_prev = theta
            theta = tp_iteration_update(T, theta)
            #print(theta)
            #print('Frobenius norm of theta-update : ', tensor3_frobenius(theta - theta_prev))
        thetas[tau] = theta
        tm_vals[tau] = tensor3_evaluate(T, theta)

    tau_max = np.argmax(tm_vals)
    #print('tau max : ', tau_max)
    theta_cap = thetas[tau_max]
    for i in range(0, N):
        theta_cap = tp_iteration_update(T, theta_cap)
    lambda_cap = tensor3_evaluate(T, theta_cap)
    return (lambda_cap, theta_cap)

# The main program
if __name__ == "__main__":
    print('Robust Tensor Decomposition Method')
    print('------------------------------------')

    random.seed()
    k = 3
    order = (k, k, k)
    T = gen_symmetric_tensor(order)

    print('Original tensor:')
    print(T)

    T_cap = np.copy(T)
    S = np.zeros(order)

    L = 20
    N = 100
    r = 20
    for iteration in range(1, r + 1):
        (lambda_cap, theta_cap) = robust_tensor_decomposition(T_cap, k, L, N)
        r = lambda_cap * tensor3_product(theta_cap)
        T_cap -= r
        S += r

    print('Tensor approximation:')
    print(S)

    print('Residual:')
    print(T - S)

    print('Symmetry:')
    print(check_symmetry(S, order))

    print('Residual Frobenius Norm:')
    print(tensor3_frobenius(T - S))
