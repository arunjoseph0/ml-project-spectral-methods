The objective of this project is the experimental analysis and comparison of
two parameter estimation techniques for latent variable models - Excess Corre-
lation Analysis (ECA) and Tensor Decomposition.