import numpy as np

def tensor_reduction(M2, M3, k):
	# Paper mandates W in R^{d*k} and M3_tilde in R^{k*k*k}.
	# So we take largest k positive eigenvalues (rest of the
	# eigenvalues have very low magnitude and shoulld be 0
	
	sigma, U = np.linalg.eigh(M2)
	d = len(sigma)
	# Take top-k eigenvalues and corresponding eigenvectors
	sigma_p = sigma[d - k : d]
	U_p = U[:, d - k : d]
	
	D = np.diag(sigma_p**(-0.5))
	W = np.matmul(U_p, D)
	n = np.size(M3, 0)
	M3_tilde = np.zeros((k, k, k))

	B = np.linalg.pinv(np.transpose(W)) # B is the Moore-Penrose pseudo-inverse of W'
	
	for i1 in range(0, k):
		for i2 in range(0, k):
			for i3 in range(0, k):
				for j1 in range(0, n):
					for j2 in range(0, n):
						for j3 in range(0, n):
							M3_tilde[i1][i2][i3] += M3[j1][j2][j3] * W[j1][i1] * W[j2][i2] * W[j3][i3]
	return M3_tilde, B
