import numpy as np
from generateGMM import *

# Generates the matrix pairs for ECA algorithm from the data X (for a document)
# Refer Anandkumar et al. 2013(A spectral algorithm for LDA), Section 4.1
def findPairsGMM(X, mean):
    d = np.size(X, 1) # dictionary size
    n = np.size(X, 0) # no of words in the document
    pairs = np.dot(X.T - mean.T, X - mean)
    pairs = pairs / (1.0*n*n)
    return pairs

# Generates the matrix triples for ECA algorithm from the data X (for a document)
# Refer Anandkumar et al. 2013(A spectral algorithm for LDA), Section 4.1
def findTriplesGMM(X, mean, eta):
    d = np.size(X, 1) # dictionary size
    n = np.size(X, 0) # no of words in the document
    triples = np.dot(X.T - mean.T, X - mean)#pairs
    sum = 0
    for k in range(0,n):
        sum += np.asscalar(np.dot(X[k]-mean, eta.T))
    triples = triples*sum/(1.0*n*n*n);
    return triples

# ECA algorithm
# Refer Anandkumar et al. 2013(A spectral algorithm for LDA), Algorithm 1
def ecaGMM(X_list, k):
    documentCount = len(X_list)  # No of documents
    d = np.size(X_list[0][0])  # Dictionary size
    mean = np.zeros(d)
    pairs = np.zeros((d, d))
    triples = np.zeros((d, d))

    #find mean
    for i in range(0, documentCount):
        X = X_list[i]
        n = np.size(X, 0)  # No of words in document i
        mean = np.add(mean, np.sum(X, axis=0) / n)
    mean = mean / documentCount
    mean = np.matrix(mean)
    print('\n\n------------------------------------------------------------------------')
    print('Estimated mean : ', mean)

    #find pairs
    for i in range(0, documentCount):
        X = X_list[i]
        pairs = np.add(pairs, findPairsGMM(X, mean))
    pairs = pairs / documentCount
    pairs = np.matrix(pairs);

    print('\n\n-------------------------------------------------------------------------')
    print('Estimated pairs : ', pairs)

    theta = np.random.randn(k)  #kx1

    #Step 1
    randTheta = np.random.randn(d, k) #dxk
    U = np.dot(pairs, randTheta)  #dxk

    #Step 2
    Ut = U.transpose()  #kxd
    Y = np.dot(np.dot(Ut,pairs),U) #kxk
    sigma, V1 = np.linalg.eig(Y)
    print('\n\nSigma : ', sigma)
    Vmul = np.dot(V1,np.diag(np.sqrt(sigma)))
    V = np.transpose(np.linalg.pinv(Vmul))
    W = np.dot(U, V)  #dxk

    #step 3
    eta = np.dot(W, theta) #dx1

    #find triples
    for i in range(0, documentCount):
        X = X_list[i]
        triples = findTriplesGMM (X, mean, eta)
    triples = triples / documentCount

    Wt = W.transpose()  #kxd
    svMat = np.dot(np.matmul(Wt, triples),W) #kxk
    tU, ts, tV = np.linalg.svd(svMat)
    leftSV, SV, rightSV = np.linalg.svd(svMat)

    #step 4
    pseudoWt = np.linalg.pinv(W).T
    O = []
    for E in leftSV:
        O_col = np.dot(pseudoWt, E.T)
        O.append(O_col)

    return np.array(O).T

if __name__ == '__main__':
    topicCount = 3
    dictionarySize = 30
    maxDocSize = 50
    minDocSize =  50
    documentCount = 200
    documentList = []

    print('ECA: GMM')
    print('---------------------------------')

    print('No of topics: ', topicCount)
    print('No of words in vocab: ', dictionarySize)
    print('No of documents: ', documentCount)
    print('No of words/doc: ', minDocSize, '...', maxDocSize)

    (mixtureDistribution, means, variance) = generateGMMParams(topicCount, dictionarySize)
    for docID in range(0, documentCount):
        documentList.append(generateGMMWords(mixtureDistribution, means, variance, topicCount, maxDocSize, maxDocSize))

    print('Mixture Distribution : ', mixtureDistribution)
    print('Mean : ', means.T)
    print('Variance : ', variance)

    O = ecaGMM(documentList, topicCount)
    print('\n\n--------------------------------------------------------------------------')
    print('Estimated distribution : ', O)