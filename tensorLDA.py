import numpy as np

#generates the matrix E[x * x] for LDA
#Refer Theorem 3.5
def generate_tensor_pairs_LDA(X):
    d = len(X[0]); # dictioanry size
    n = len(X); # no of words in the document
    pairs = np.zeros((d,d));
    for i in range(0, n):
         for j in range(0, n):
             pairs = pairs + np.add(pairs, np.outer(X[i], X[j]));
    pairs = pairs / (n*n);
    return pairs;

#generates the matrix M2 for LDA
#Refer Theorem 3.5
def generate_M2_LDA(X, M1, alpha0):
    pairs = generate_tensor_pairs_LDA(X);
    M2 = pairs - (alpha0/(alpha0+1)) * np.tensordot(M1, M1, axes = 0);
    return M2;

#calculates M2 matrix from the word distributions mu obtained from LDA
def calculate_M2(MU, k, alpha):
    d = len(MU[0]);
    alpha0 = sum(alpha); #not sure if alpha0 has to be computed this way, or to use the alpha0 used for estimation of M2
    M2 = np.zeros((d, d));
    for i in range(0, k):
        M2 = M2 + (alpha[i]/((alpha0 + 1) * alpha0)) * np.tensordot(MU[i], MU[i], axes = 0);
    return M2;

#generates the matrix M3 for LDA
#Refer Theorem 3.5
def generate_M3_LDA(X, M1, alpha0):
    d = len(X[0]) # dictioanry size
    n = len(X) # no of words in the document
    M3 = np.zeros((d, d, d))
    for i in range(0,n):
        for j in range(0,n):
            for k in range(0,n):
                M3 = M3 + np.tensordot(np.tensordot(X[i], X[j], axes = 0), X[k], axes = 0);
    M3 = M3 / (n*n*n);
    term1 = np.zeros((d, d, d));
    for i in range(0, d):
        ei = np.zeros(d);
        ei[i] = 1;
        term1 = term1 + np.tensordot(np.tensordot(M1, ei, axes = 0), ei, axes = 0);
        term1 = term1 + np.tensordot(np.tensordot(ei, M1, axes = 0), ei, axes = 0);
        term1 = term1 + np.tensordot(np.tensordot(ei, ei, axes = 0), M1, axes = 0);
    term2 = np.tensordot(np.tensordot(M1, M1, axes = 0), M1, axes = 0);

    M3 = M3 - (alpha0 /(alpha0 + 2)) * term1 + (2 * alpha0 * alpha0/((alpha0 + 2) * (alpha0 + 1))) * term2 ;
    return M3;

#calculates M3 matrix from the word distributions mu obtained from LDA and the LDA parameters
def calculate_M3(MU, k, alpha):
    d = len(MU[0]);
    alpha0 = sum(alpha); #not sure if alpha0 has to be computed this way, or to use the alpha0 used for estimation of M2
    M3 = np.zeros((d, d, d));
    for i in range(0, k):
        M3 = M3 + (2*alpha[i]/((alpha0 + 2) * (alpha0 + 1) * alpha0)) *  np.tensordot(np.tensordot(MU[i], MU[i], axes = 0), MU[i], axes = 0);
    return M3;

def generate_input_matrices_LDA(X, alpha0):
    d = len(X[0]);
    n = len(X);
    M1 = X.sum(axis = 0)/ n;
    tensor_pairs = generate_tensor_pairs_LDA(X);
    M2 = generate_M2_LDA(X, M1, alpha0);
    M3 = generate_M3_LDA(X, M1, alpha0);
    return M3, M2, M1;