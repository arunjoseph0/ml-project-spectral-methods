import numpy as np
from random import randint

def generateGMMParams(mixturesCount, gaussianDimension):
	mixtureDistribution = np.ones(mixturesCount) / mixturesCount
	means  = []
	
	for topicID in range(0, mixturesCount):
		mean = np.random.random(gaussianDimension)
		means.append(mean)
	variance = 1 #np.random.random()
	return (mixtureDistribution, np.array(means), variance)

def generateGMMWords(mixtureDistribution, means, variance, mixturesCount, minDocSize, maxDocSize):
	documentSize      = randint(minDocSize, maxDocSize)
	gaussianDimension = np.size(means[0])
	
	covarianceMatrix  = variance * np.identity(gaussianDimension)
	x_vecs = []
	for wordPos in range(0, documentSize):
		selectedMixture = np.random.choice(np.arange(0, mixturesCount), p = mixtureDistribution)
		mean = means[selectedMixture]
		x = np.random.multivariate_normal(mean, covarianceMatrix, 1)
		x_vecs.append(np.ndarray.flatten(x))
	
	return  np.array(x_vecs)

if __name__ == '__main__':
	mixturesCount     = 5
	gaussianDimension = 8
	maxDocSize = 10
	
	(mixtureDistribution, means, variance) = generateGMMParams(mixturesCount, gaussianDimension)
	x_vecs = generateGMMWords(mixtureDistribution, means, variance, mixturesCount, maxDocSize, maxDocSize)
	print('Params: No of mixtures: ', mixturesCount, ' Dimension of Gaussian: ', gaussianDimension, 'Document size: ', maxDocSize)
	print('Mixtures distribution (W):', mixtureDistribution)
	print('Variance (sigma^2):', variance)
	print('Means (Mu_1,...,Mu_k): ', means)
	
	print('Words: ')
	print(x_vecs)
